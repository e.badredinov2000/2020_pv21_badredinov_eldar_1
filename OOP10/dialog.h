#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <vector>
#include <string>
#include <algorithm>
#include <sstream>
#include <iostream>
namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();
    QString x;//�������� ����
    QString y;//��������� ����
    std::vector <QString> dirs;// ������ ������
    void SetDir (QString x,QString y);
    int SafeCopy(char* inFldr, char* outFldr);
private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::Dialog *ui;
};

#endif // DIALOG_H
