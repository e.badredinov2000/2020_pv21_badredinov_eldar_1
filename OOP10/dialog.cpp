#include "dialog.h"
#include "ui_dialog.h"
#include <fstream>
#include "SmartPointer.h"
using namespace std;
Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::SetDir (QString x,QString y)
{
       this->x = x; //�������� �����
       this->y = y; // ����� ����������
}

int Dialog::SafeCopy(char* inFldr, char* outFldr)
{
        int len = 4096; //�������� ������ ������
        char* buffer =new char[len];

        ifstream infile(inFldr, ios::in | ios::binary);
        ofstream outfile(outFldr, ios::out | ios::binary);
        if(!infile || !outfile){ //������ �������� ������
            cout << "Oops...\n";
            return 1;
        }

        while (!infile.eof()){
            infile.read(buffer, len);
            if(infile.gcount()) outfile.write(buffer, infile.gcount());
            //gcount ���������� ���������� ����, ��������� � ��������� ���
            //�� � ���������� ��� ��������, ��� ���-�� ���������, � ������ ������������� ���������� ������������ ����
        }

        infile.close();
        outfile.close(); //�������� ������
        delete[] buffer;// �������� ������
        return 0;

}


// ����� ������ ���������� ���������� ��� ������ ����������� �������� � ������
// ����� �������� system("copy ") �� ����� ������ �� ���������� x � ���������� y
void Dialog::on_pushButton_clicked()//������� ����������, �� �� ���������� ������ ��� windows
{
    QString dirs = ui ->lineEdit->text(); // ��������� ����� �� ����
    QByteArray ba = dirs.toLocal8Bit(); // ��������� � ������ ���
    SmartPointer<char*> c_str2 = new char* (ba.data()); //��������� � char *
    SmartPointer<char*> f = new char*( strtok(*c_str2," ")) ;
    while ( *f )// ���������� �� �������
       {
        this->dirs.push_back(QString::fromLocal8Bit(*f));
        std::cout << *f << std::endl ;
        *f=strtok(NULL," ") ;
       }
    for (std::vector<QString>::iterator it = this->dirs.begin() ; it!=this->dirs.end() ; ++it){//���� ���� ��������������� �����
        QString cmd="copy ";
        cmd = cmd + this->x + (*it) + ' ' + this->y;// ��������� ������� ��� �����������
        QByteArray ba2 = cmd.toLocal8Bit();//�������� � ������� ���
        SmartPointer<char*> c_str3 = new char* (ba2.data());// �������� � char*
        system(*c_str3);//�������� ���������� �������
    }
}

void Dialog::on_pushButton_2_clicked()//����� ������, �� ���������� ���������� ��� ������������� cmd, �����������������
{
    QString dirs = ui ->lineEdit->text(); // ��������� ����� �� ����
    QByteArray ba = dirs.toLocal8Bit();// ��������� � ������ ���
    SmartPointer<char*> c_str2 = new char* (ba.data());//��������� � char *
    SmartPointer<char*> f = new char*( strtok(*c_str2," ")) ;
    while ( *f ) // ��������� �� �������
       {
        this->dirs.push_back(QString::fromLocal8Bit(*f));
          //std::cout << f << std::endl ;
         *f=strtok(NULL," ") ;
       }
    QString x,y;
    for (std::vector<QString>::iterator it = this->dirs.begin() ; it!=this->dirs.end() ; ++it){// ���� � ������� ���� ��������������� �����
        x=this->x;
        y=this->y;
        x+=(*it);// ��������� � ���� ��������� ����
        y+=(*it);
        QByteArray bax = x.toLocal8Bit();//��������� � ������ ���
        QByteArray bay = y.toLocal8Bit();
        SmartPointer<char*> c_strx =  new char* (bax.data());//��������� � char *
        SmartPointer<char*> c_stry = new char* (bay.data());
        SafeCopy(*c_strx,*c_stry);// ��������

    }
}
